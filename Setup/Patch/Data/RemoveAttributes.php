<?php
namespace Avanti\DeleteEavAttributes\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Psr\Log\LoggerInterface;

class RemoveAttributes implements DataPatchInterface
{
    private $moduleDataSetup;
    private $categorySetupFactory;
    private $logger;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CategorySetupFactory $categorySetupFactory,
        LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->logger = $logger;
    }

    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $categorySetup = $this->categorySetupFactory->create(['setup' => $this->moduleDataSetup]);
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
        $allAttributes = ['show_products','nbr_product_value','use_static_block','use_static_block_top','static_block_top_value','use_static_block_left',
            'static_block_left_value','use_static_block_bottom','static_block_bottom_value','use_static_block_right','static_block_right_value','use_label','label_value',
            'level_column_count','use_thumbail','disabled_children'];

        foreach ($allAttributes as $attribute) {
            try {
                $categorySetup->removeAttribute($entityTypeId, $attribute);
            } catch (\Exception $e) {
                $this->logger->error("O atributo : $attribute não foi encontrado");
            }
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
